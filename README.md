# Unity

Look into Unity and maybe scripting in Rust.

[2018.1](https://www.youtube.com/watch?v=fRHMB4MWSFA)

## Shaders

- [Funky flame tut](https://static.ciaccodavi.de/old/games/shader-test/tutorial/)

## Links

- [PluralSight path](https://app.pluralsight.com/paths/skills/unity-game-development-core-skills)
- [PluralSight course](https://store.unity.com/configure-plan-ala/swords-shovels)
- [Interactive Tutorials](https://unity3d.com/learn/tutorials/projects/interactive-tutorials/play-edit-mode?playlist=49382)
- [More Tutorials](https://unity3d.com/learn/tutorials)
- [Top Assets](https://assetstore.unity.com/plists/hottest-packages-46926)
- [Beginner Assets](https://assetstore.unity.com/lists/musthave-beginner-assets-24505)
- [Asset Store](https://assetstore.unity.com/)

## VS Code

- [Unity Development with VS Code](https://code.visualstudio.com/docs/other/unity)
- [Debugger for Unity](https://marketplace.visualstudio.com/items?itemName=Unity.unity-debug)
- [VS Code Integration](https://www.assetstore.unity3d.com/en/#!/content/45320)
    - [Source on GitHub](https://github.com/dotBunny/VSCode)

- [(2017) Unity development on macOS with Visual Studio Code](https://pawelgrzybek.com/unity-development-on-macos-with-visual-studio-code/)
- [(2016) Using Visual Studio Code with Unity](http://blog.theknightsofunity.com/using-visual-studio-code-with-unity/)
- [(2015) How to use Visual Studio Code as Unity3d script editor](http://laumania.net/2015/04/30/how-to-use-visual-studio-code-as-unity3d-script-editor/)
